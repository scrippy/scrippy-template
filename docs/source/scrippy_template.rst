scrippy\_template package
=========================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   scrippy_template.template

Module contents
---------------

.. automodule:: scrippy_template
   :members:
   :undoc-members:
   :show-inheritance:
